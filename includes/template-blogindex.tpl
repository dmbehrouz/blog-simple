<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http:"//www.w3.org/TR/xhtml1/DTD/xhtm1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>{$title}</title>
<link rel="stylesheet" href="includes/style.css" type="text/css" media="screen" charset="utf-8" />
</head>
<body>

<div id="main">
<h1>{$header}</h1>
<div id="blogPosts">

{$posts}

</div>
</div>

</body>
</html>

