
<?php
include ("includes/includes.php");
include ("includes/template-engine.php");

$blogPosts = GetBlogPosts();
$template  = new template();

$html='';
foreach ($blogPosts as $post)
{
$html.= "<div class='post'>";
$html.= "<h2>" . $post->title . "</h2>";
$html.= "<p>" . $post->post . "</p>";
$html.= "<span class='footer'>Posted By: " . $post->author . " Posted on: " . $post->datePosted . " Tags: " . $post->tags . "</span>";
$html.= "</div>";
}
$template->assign(array(
    'title'   => 'My Simple Blog !',
    'header'  => 'UME blog !',
    'posts' => $html
	));

$template->display('includes/template-blogindex.tpl');
?>

